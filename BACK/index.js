require ('dotenv').config();

const express = require('express');
const app = express();
require('./database');
const validaToken = require('./middleware/middleware.validate-token');

const cors = require('cors');
var corsOptions = {
    origin: '*', // Reemplazar con dominio
    optionsSuccessStatus: 200 // 
}
app.use(cors(corsOptions));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());


app.use('/api/user',validaToken,require('./routes/routes.user'));
app.use('/api/auth',require('./routes/routes.login'));
app.use('/api/externa',validaToken,require('./routes/routes.apiexterna'));



app.listen(process.env.PORT, function () {
    console.log('app listening at port %s',process.env.PORT);
});